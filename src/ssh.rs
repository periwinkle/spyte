use crate::config::Config;
use anyhow::{Context, Result};
use ssh2::{Channel, Session};
use std::{
    io::{BufRead, BufReader},
    net::TcpStream,
};

pub struct SshSession {
    chan: BufReader<Channel>,
}

impl SshSession {
    pub fn connect(config: &Config) -> Result<Self> {
        let stream = TcpStream::connect(&config.target_addr)
            .context(format!("Failed to connect to {}", config.target_addr))?;
        let mut sess = Session::new()?;
        sess.set_tcp_stream(stream);
        sess.handshake().context("Could not handshake with host")?;
        sess.userauth_password(&config.username, &config.password)?;
        let mut chan = sess.channel_session()?;
        chan.exec("sudo pkill -9 usb-mitm; sudo usb-mitm -z 2>/dev/null")
            .context("Could not execute command on host")?;
        Ok(Self {
            chan: BufReader::new(chan),
        })
    }
    pub fn read_frame(&mut self, buf: &mut String) {
        self.chan.read_line(buf).unwrap();
    }
    pub fn disconnect(self) -> Result<()> {
        let mut c = self.chan.into_inner();
        c.send_eof()?;
        c.close()?;
        Ok(())
    }
}
