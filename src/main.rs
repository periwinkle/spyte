mod cli;
mod config;
mod packet;
mod poll;
mod skin;
mod ssh;

use clap::Parser;
use cli::Args;
use config::Config;
use crossbeam_channel::unbounded;
use crossbeam_queue::ArrayQueue;
use crow::{
    glutin::{
        dpi::PhysicalSize,
        event::{Event, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        window::WindowBuilder,
    },
    Context, DrawConfig,
};
use poll::Message;
use skin::{RenderSkin, Skin};
use ssh::SshSession;
use std::sync::Arc;

fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    let mut cfg = Config::open()?;
    cfg.set_args(args);
    let sess = SshSession::connect(&cfg)?;
    let (skin, asset_path) = Skin::open(&cfg.skin)?;

    let (main_tx, poll_rx) = unbounded();
    let (poll_tx, main_rx) = unbounded();
    let el = EventLoop::new();
    let w = WindowBuilder::new()
        .with_resizable(false)
        .with_title("spyte");
    let mut ctx = Context::new(w, &el)?;
    let skin = RenderSkin::from_skin(&mut ctx, skin, &asset_path)?;
    ctx.window()
        .set_inner_size(PhysicalSize::<u32>::from(skin.size()));
    let draw_conf = DrawConfig::default();
    let q = Arc::new(ArrayQueue::new(10));
    poll::poll_thread(poll_tx, poll_rx, Arc::clone(&q), sess);
    main_tx.send(Message::FrameRequest)?;
    el.run(move |ev: Event<()>, _, flow: &mut ControlFlow| {
        *flow = ControlFlow::Poll;
        match ev {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                main_tx.send(Message::Stop).unwrap();
                *flow = ControlFlow::Exit;
            }
            Event::MainEventsCleared => {
                if main_rx.try_recv().is_ok() {
                    ctx.window().request_redraw();
                }
            }
            Event::RedrawRequested(_) => {
                //println!("{}", a.elapsed().as_micros());
                //a = std::time::Instant::now();
                let mut sur = ctx.surface();
                if let Some(frame) = q.pop() {
                    ctx.clear_color(&mut sur, (0., 0., 0., 1.));
                    //ctx.draw(&mut sur, &skin.background, (0, 0), &draw_conf);
                    //:println!("frame {}", frame);
                    let to_draw = skin.to_draw(&frame);
                    for button in to_draw {
                        ctx.draw(&mut sur, &button.image, button.pos.into(), &draw_conf);
                    }
                }
                ctx.present(sur).unwrap();
            }
            _ => {}
        }
    });
}
