use crate::packet::InputFrame;
use crate::ssh::SshSession;
use crossbeam_channel::{Receiver, Sender};
use crossbeam_queue::ArrayQueue;
use std::sync::Arc;
use std::time::Instant;
use std::{
    str::FromStr,
    thread::{self, JoinHandle},
};

#[derive(Debug)]
pub enum Message {
    FrameRequest,
    Frame(InputFrame),
    Stop,
}

pub fn poll_thread(
    tx: Sender<Message>,
    rx: Receiver<Message>,
    q: Arc<ArrayQueue<InputFrame>>,
    mut sess: SshSession,
) -> JoinHandle<()> {
    thread::spawn(move || {
        let mut frame = String::with_capacity(58);
        let mut last = InputFrame::default();
        loop {
            let a = Instant::now();
            sess.read_frame(&mut frame);
            let new = InputFrame::from_str(&frame).unwrap_or_default();
            if last.buttons != new.buttons {
                println!(
                    "b {} {:?}",
                    a.elapsed().as_micros(),
                    &last.buttons ^ &new.buttons
                );
                last = new.clone();
                tx.send(Message::FrameRequest).unwrap();
                q.force_push(new);
            }
            if let Ok(m) = rx.try_recv() {
                match m {
                    Message::Stop => break,
                    _ => {}
                }
            }
            frame.clear();
        }
        sess.disconnect().unwrap(); // too bad!
    })
}
