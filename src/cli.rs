use clap::Parser;

#[derive(Parser, Debug)]
pub struct Args {
    #[clap(short, long)]
    pub target_addr: Option<String>,
    #[clap(short, long)]
    pub username: Option<String>,
    #[clap(short, long)]
    pub password: Option<String>,
    #[clap(short, long)]
    pub skin: Option<String>,
}
