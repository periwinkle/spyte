use std::{
    collections::{HashMap, HashSet},
    fmt::{Display, Formatter, Result as FmtRes},
    str::FromStr,
};

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub enum SwitchButton {
    A,
    B,
    X,
    Y,
    Plus,
    Minus,
    Home,
    Cap,
    Zl,
    Zr,
    L,
    R,
    Up,
    Down,
    Left,
    Right,
    Ls,
    Rs,
}

#[derive(Debug, Clone)]
pub struct InputFrame {
    pub buttons: HashSet<SwitchButton>,
    lstick: StickState,
    rstick: StickState,
    created: std::time::Instant,
}

#[derive(Debug, Clone)]
pub struct StickState {
    x: f32,
    y: f32,
}

pub enum ParseFrameErr {
    WrongFrameLength,
    ParseInt(std::num::ParseIntError),
}

fn map_stick_val(val: u8) -> f32 {
    if val < 127 {
        f32::from(val) / 128.
    } else {
        f32::from(255 - val) / -128.
    }
}

impl FromStr for InputFrame {
    type Err = ParseFrameErr;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use SwitchButton::*;
        if s.len() != 57 {
            return Err(ParseFrameErr::WrongFrameLength);
        }
        let b = s.chars().skip(24);
        let mut cnt = 0;
        let mut vals = [String::new(), String::new(), String::new(), String::new()];
        for c in b.take(32) {
            vals[cnt / 8].push(c);
            cnt += 1;
        }
        let vals = vals
            .iter()
            .map(|v| v.chars().rev().collect::<String>())
            .collect::<Vec<_>>();
        let lx =
            map_stick_val(u8::from_str_radix(&vals[0], 2).map_err(|e| ParseFrameErr::ParseInt(e))?);
        let ly =
            map_stick_val(u8::from_str_radix(&vals[1], 2).map_err(|e| ParseFrameErr::ParseInt(e))?);
        let rx =
            map_stick_val(u8::from_str_radix(&vals[2], 2).map_err(|e| ParseFrameErr::ParseInt(e))?);
        let ry =
            map_stick_val(u8::from_str_radix(&vals[3], 2).map_err(|e| ParseFrameErr::ParseInt(e))?);
        let mut r = Self::default();
        let s = s.as_bytes();
        let mut h: HashMap<SwitchButton, _> = HashMap::new();
        h.insert(Y, s[0] == 0x31);
        h.insert(X, s[1] == 0x31);
        h.insert(B, s[2] == 0x31);
        h.insert(A, s[3] == 0x31);
        h.insert(R, s[6] == 0x31);
        h.insert(Zr, s[7] == 0x31);
        h.insert(Minus, s[8] == 0x31);
        h.insert(Plus, s[9] == 0x31);
        h.insert(Rs, s[10] == 0x31);
        h.insert(Ls, s[11] == 0x31);
        h.insert(Home, s[12] == 0x31);
        h.insert(Cap, s[13] == 0x31);
        h.insert(Down, s[16] == 0x31);
        h.insert(Up, s[17] == 0x31);
        h.insert(Right, s[18] == 0x31);
        h.insert(Left, s[19] == 0x31);
        h.insert(L, s[22] == 0x31);
        h.insert(Zl, s[23] == 0x31);
        r.buttons = h
            .iter()
            .filter_map(|(&k, &v)| if v { Some(k) } else { None })
            .collect::<HashSet<_>>();
        r.lstick = StickState { x: lx, y: ly };
        r.rstick = StickState { x: rx, y: ry };
        Ok(r)
    }
}

impl Display for InputFrame {
    fn fmt(&self, f: &mut Formatter) -> FmtRes {
        write!(f, "{}    ", self.created.elapsed().as_micros());
        for button in &self.buttons {
            write!(f, "{:?} ", button)?;
        }
        write!(f, " l {} {}", self.lstick.x, self.lstick.y)?;
        write!(f, " r {} {}", self.rstick.x, self.rstick.y)?;
        writeln!(f)
    }
}

impl Default for InputFrame {
    fn default() -> Self {
        Self {
            buttons: HashSet::new(),
            lstick: StickState { x: 0., y: 0. },
            rstick: StickState { x: 0., y: 0. },
            created: std::time::Instant::now(),
        }
    }
}
